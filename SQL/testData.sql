USE `setlWFL`;
CREATE USER 'newuser'@'localhost' IDENTIFIED BY 'password';
GRANT SELECT ON * . * TO 'newuser'@'localhost';
FLUSH PRIVILEGES;

select * from tblCmds
CALL sp_tblPermissionAreas_Insert(123456789102345,
0,
1,
'CreateSnap',
'{"Phase":"Balance","Assets":"BP|BPA","IncludeRow":"Balance>0","ColSource":"Address, Balance","ExcludeRow":"Address=123456789102345","Name":"Bal","ColHead":"Address, Balance"}',
1,
"2017-01-17 19:06:09",
NULL,
FALSE);

CALL `sp_tblPermissionAreas_Insert` (133456789,98765,1,"CreateSnap",'{"Name" : "Bal","Phase" : "Balance","Asset" : "Assets.Base[0]","ColHead" : "Address,Balance","ColSource" : "Address,Balance","ExcludeRow" : "Address=133456789","IncludeRow" : "Balance>0"}',NULL,"2017-01-12 16:42:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (133456789,98765,2,"IssueAsset",'{"params" : "Some parameters here!"}',1,"2017-01-12 16:42:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (143456789,98765,1,"CreateSnap",'{"Name" : "Bal","Phase" : "Balance","Asset" : "Assets.Base[0]","ColHead" : "Address,Balance","ColSource" : "Address,Balance","ExcludeRow" : "Address=143456789","IncludeRow" : "Balance>0"}',NULL,"2017-01-13 17:54:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (143456789,98765,2,"IssueAsset",'{"params" : "Some parameters here!"}',1,"2017-01-13 17:54:09",NULL,FALSE);
UPDATE `tblUnitQueueCommand` SET `executed` = "2017-01-13 17:54:09" WHERE `AccountID` = 143456789 AND `unitID` = 98765 AND `step` = 2;
CALL `sp_tblPermissionAreas_Insert` (143456789,98765,3,"IssueAsset",'{"params" : "Some Other parameters here!"}',1,"2017-01-13 17:55:09",NULL,FALSE);

CALL `sp_tblPermissionAreas_Insert` (153456789,98765,1,"CreateSnap",'{"Name" : "Bal","Phase" : "Balance","Asset" : "Assets.Base[0]","ColHead" : "Address,Balance","ColSource" : "Address,Balance","ExcludeRow" : "Address=153456789","IncludeRow" : "Balance>0"}',NULL,"2017-01-14 19:06:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (153456789,98765,2,"IssueAsset",'{"params" : "Some parameters here!"}',1,"2017-01-17 19:06:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (163456789,98765,1,"CreateSnap",'{"Name" : "Bal","Phase" : "Balance","Asset" : "Assets.Base[0]","ColHead" : "Address,Balance","ColSource" : "Address,Balance","ExcludeRow" : "Address=163456789","IncludeRow" : "Balance>0"}',NULL,"2017-01-15 20:18:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (163456789,98765,2,"IssueAsset",'{"params" : "Some parameters here!"}',1,"2017-01-15 20:18:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (173456789,98765,1,"CreateSnap",'{"Name" : "Bal","Phase" : "Balance","Asset" : "Assets.Base[0]","ColHead" : "Address,Balance","ColSource" : "Address,Balance","ExcludeRow" : "Address=173456789","IncludeRow" : "Balance>0"}',NULL,"2017-01-16 21:30:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (173456789,98765,2,"IssueAsset",'{"params" : "Some parameters here!"}',1,"2017-01-17 21:30:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (183456789,98765,1,"CreateSnap",'{"Name" : "Bal","Phase" : "Balance","Asset" : "Assets.Base[0]","ColHead" : "Address,Balance","ColSource" : "Address,Balance","ExcludeRow" : "Address=183456789","IncludeRow" : "Balance>0"}',NULL,"2017-01-17 22:42:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (183456789,98765,2,"IssueAsset",'{"params" : "Some parameters here!"}',1,"2017-01-17 22:42:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (193456789,98765,1,"CreateSnap",'{"Name" : "Bal","Phase" : "Balance","Asset" : "Assets.Base[0]","ColHead" : "Address,Balance","ColSource" : "Address,Balance","ExcludeRow" : "Address=193456789","IncludeRow" : "Balance>0"}',NULL,"2017-01-18 23:54:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (193456789,98765,2,"IssueAsset",'{"params" : "Some parameters here!"}',1,"2017-01-18 23:54:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (203456789,98765,1,"CreateSnap",'{"Name" : "Bal","Phase" : "Balance","Asset" : "Assets.Base[0]","ColHead" : "Address,Balance","ColSource" : "Address,Balance","ExcludeRow" : "Address=203456789","IncludeRow" : "Balance>0"}',NULL,"2017-01-20 01:06:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (203456789,98765,2,"IssueAsset",'{"params" : "Some parameters here!"}',1,"2017-01-20 01:06:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (213456789,98765,1,"CreateSnap",'{"Name" : "Bal","Phase" : "Balance","Asset" : "Assets.Base[0]","ColHead" : "Address,Balance","ColSource" : "Address,Balance","ExcludeRow" : "Address=213456789","IncludeRow" : "Balance>0"}',NULL,"2017-01-21 02:18:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (213456789,98765,2,"IssueAsset",'{"params" : "Some parameters here!"}',1,"2017-01-21 02:18:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (223456789,98765,1,"CreateSnap",'{"Name" : "Bal","Phase" : "Balance","Asset" : "Assets.Base[0]","ColHead" : "Address,Balance","ColSource" : "Address,Balance","ExcludeRow" : "Address=223456789","IncludeRow" : "Balance>0"}',NULL,"2017-01-22 03:30:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (223456789,98765,2,"IssueAsset",'{"params" : "Some parameters here!"}',1,"2017-01-22 03:30:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (233456789,98765,1,"CreateSnap",'{"Name" : "Bal","Phase" : "Balance","Asset" : "Assets.Base[0]","ColHead" : "Address,Balance","ColSource" : "Address,Balance","ExcludeRow" : "Address=233456789","IncludeRow" : "Balance>0"}',NULL,"2017-01-23 04:42:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (233456789,98765,2,"IssueAsset",'{"params" : "Some parameters here!"}',1,"2017-01-23 04:42:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (243456789,98765,1,"CreateSnap",'{"Name" : "Bal","Phase" : "Balance","Asset" : "Assets.Base[0]","ColHead" : "Address,Balance","ColSource" : "Address,Balance","ExcludeRow" : "Address=243456789","IncludeRow" : "Balance>0"}',NULL,"2017-01-24 05:54:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (243456789,98765,2,"IssueAsset",'{"params" : "Some parameters here!"}',1,"2017-01-24 05:54:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (253456789,98765,1,"CreateSnap",'{"Name" : "Bal","Phase" : "Balance","Asset" : "Assets.Base[0]","ColHead" : "Address,Balance","ColSource" : "Address,Balance","ExcludeRow" : "Address=253456789","IncludeRow" : "Balance>0"}',NULL,"2017-01-25 07:06:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (253456789,98765,2,"IssueAsset",'{"params" : "Some parameters here!"}',1,"2017-01-25 07:06:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (263456789,98765,1,"CreateSnap",'{"Name" : "Bal","Phase" : "Balance","Asset" : "Assets.Base[0]","ColHead" : "Address,Balance","ColSource" : "Address,Balance","ExcludeRow" : "Address=263456789","IncludeRow" : "Balance>0"}',NULL,"2017-01-26 08:18:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (263456789,98765,2,"IssueAsset",'{"params" : "Some parameters here!"}',1,"2017-01-26 08:18:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (273456789,98765,1,"CreateSnap",'{"Name" : "Bal","Phase" : "Balance","Asset" : "Assets.Base[0]","ColHead" : "Address,Balance","ColSource" : "Address,Balance","ExcludeRow" : "Address=273456789","IncludeRow" : "Balance>0"}',NULL,"2017-01-27 09:30:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (273456789,98765,2,"IssueAsset",'{"params" : "Some parameters here!"}',1,"2017-01-27 09:30:09",NULL,FALSE);

update `tblUnitQueueCommand` SET `executed` = now() where `accountID` = 173456789 AND `unitID`=98765 AND `step`=1;
DELETE FROM `tblUnitQueueCommand` WHERE `accountID` != 1;
GRANT ALL ON setlWFL to 'newuser'@'localhost';

CALL `sp_tblPermissionAreas_Insert` (283456789,98765,1,"CreateSnap",'{"Name" : "Bal","Phase" : "Balance","Asset" : "Assets.Base[0]","ColHead" : "Address,Balance","ColSource" : "Address,Balance","ExcludeRow" : "Address=283456789","IncludeRow" : "Balance>0"}',1,"2017-01-28 10:42:09",NULL,TRUE);
CALL `sp_tblPermissionAreas_Insert` (283456789,98765,2,"IssueAsset",'{"params" : "Some parameters here!"}',1,"2017-01-28 10:42:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (283456789,98765,3,"CreateSnap",'{"Name" : "Bal","Phase" : "Balance","Asset" : "Assets.Base[0]","ColHead" : "Address,Balance","ColSource" : "Address,Balance","ExcludeRow" : "Address=283456789","IncludeRow" : "Balance>0"}',3,"2017-01-28 10:42:09",NULL,TRUE);
CALL `sp_tblPermissionAreas_Insert` (283456789,98765,4,"IssueAsset",'{"params" : "Some parameters here!"}',3,"2017-01-28 10:42:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (283456789,98765,5,"CreateSnap",'{"Name" : "Bal","Phase" : "Balance","Asset" : "Assets.Base[0]","ColHead" : "Address,Balance","ColSource" : "Address,Balance","ExcludeRow" : "Address=283456789","IncludeRow" : "Balance>0"}',3,"2017-01-28 10:42:09",NULL,FALSE);
CALL `sp_tblPermissionAreas_Insert` (283456789,98765,6,"IssueAsset",'{"params" : "Some parameters here!"}',3,"2017-01-28 10:42:09",NULL,FALSE);

UPDATE `tblUnitQueueCommand` SET executed = now() WHERE STEP < 5 AND accountID = '283456789';

DROP TABLE IF EXISTS `tempNotExecuted`;
CREATE  TABLE `tempNotExecuted` (`rollBackStep` INT) AS SELECT `accountID`, `unitID`,min(step) AS minimumStep from `view_CommandsForExec` GROUP BY `accountID`, `unitID`  ;

SET SQL_SAFE_UPDATES=0;
UPDATE `tempNotExecuted` SET `tempNotExecuted`.`rollBackStep` = (SELECT `tblUnitQueueCommand`.`rollbackStep` FROM `tblUnitQueueCommand` WHERE tempNotExecuted.accountID= tblUnitQueueCommand.accountID AND tempNotExecuted.unitID = tblUnitQueueCommand.unitID AND tempNotExecuted.minimumStep = tblUnitQueueCommand.step);
delete from `tempNotExecuted` WHERE `rollbackStep` IS NULL;

UPDATE tblUnitQueueCommand unitCommand
	RIGHT JOIN tempNotExecuted toBeUpdated
	ON unitCommand.accountID = toBeUpdated.accountID AND unitCommand.unitID  = toBeUpdated.unitID AND unitCommand.rollBackStep  = toBeUpdated.rollBackStep
    SET executed = null, recoveryStep = false, processing = false;
    
UPDATE tblUnitQueueCommand unitCommand
    SET processing = FALSE
	WHERE executed = NULL AND processing = true;


select * from tblUnitQueueCommand;
EXPLAIN EXTENDED SELECT * FROM tblUnitQueueCommand unitCommand
	RIGHT JOIN tempNotExecuted toBeUpdated
	ON unitCommand.accountID = toBeUpdated.accountID AND unitCommand.unitID  = toBeUpdated.unitID AND unitCommand.rollBackStep  = toBeUpdated.rollBackStep;
select * from `tempNotExecuted`;
select * from `tblUnitQueueCommand`;
delete from `tblUnitQueueCommand`;

SELECT * from view_CommandsForExec;

DROP TABLE IF EXISTS `requestsIn`;
CREATE TABLE `requestsIn` (
	`request` JSON NOT NULL,
    `received` DATETIME NOT NULL DEFAULT now(),
    `inProgress` BOOLEAN DEFAULT FALSE,
    `serialID` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `processed` BOOLEAN DEFAULT FALSE,
	PRIMARY KEY (`serialID`, `processed`)
)ENGINE = InnoDB;


SET @loadNewRequestStatement = "INSERT INTO requestsIn (`request`) VALUES (?)";
PREPARE valid3 FROM @s;

DROP PROCEDURE IF EXISTS `loadnewRequest`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `loadnewRequest` (
	thisJson JSON
    )
BEGIN
	set @newJson = thisJson;
    EXECUTE  valid3 USING @newJson;
END;

