CREATE DATABASE  IF NOT EXISTS `setlWFL` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `setlWFL`;
PREPARE validCommand FROM "SELECT EXISTS (SELECT 1 FROM `tblCmds` WHERE cmd=?) INTO @result";
CREATE USER 'newuser'@'localhost' IDENTIFIED BY 'password';
GRANT ALL ON setlWFL.* to 'newuser'@'localhost';
DROP TABLE IF EXISTS `tblCmds`;
CREATE TABLE `tblCmds` (
	`cmd` VARCHAR(20) UNIQUE NOT NULL
) Engine=InnoDB;

INSERT INTO `tblCmds` (`cmd`)  VALUES ('IssueAsset');
INSERT INTO `tblCmds` (`cmd`)  VALUES ('CreateSnap');
INSERT INTO `tblCmds` (`cmd`)  VALUES ('CreateSnap');


DROP TABLE IF EXISTS `tblUnitQueueCommand`;

CREATE TABLE `tblUnitQueueCommand` (
    `accountID` BIGINT(20) NOT NULL,
    `unitID` BIGINT(20) NOT NULL,
    `step` INT NOT NULL,
    `command` TEXT NOT NULL,
    `params` JSON,
    `rollbackStep` INT ,
    `executionTime` DATETIME NOT NULL,
    `created` DATETIME DEFAULT NOW(),
    `executed` DATETIME DEFAULT NULL,
    `dependency` INT DEFAULT NULL,
    `recoveryStep` BOOL NOT NULL DEFAULT FALSE,
    `processing` BOOL DEFAULT FALSE,
    PRIMARY KEY (`accountID` , `unitID` , `step`)
)  ENGINE = InnoDB;

CREATE INDEX recoveryStep ON `tblUnitQueueCommand` (`recoveryStep`, `executed`, `executionTime`, `processing`, `rollbackStep`);


DROP PROCEDURE IF EXISTS `sp_tblPermissionAreas_Insert`;

DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_tblPermissionAreas_Insert` (
	thisAccountID bigint(20),
	thisUnitID  bigint(20),
    thisStep INT, 
    thisCommand text,
    thisParams json,
    thisRollbackStep INT,
    thisExecutionTime DATE,
    thisDependency INT,
    thisRecoveryStep BOOL
)
BEGIN
	SET @command = thisCommand;
    PREPARE validCommand FROM "SELECT EXISTS (SELECT 1 FROM `tblCmds` WHERE cmd=?) INTO @result";
    EXECUTE validCommand USING @command;

    SELECT @result;

	IF (@result = TRUE) THEN

        SET @accountId = thisAccountID;
        SET @unitID = thisUnitID;
        SET @step = thisStep;
        SET @command = thisCommand;
        SET @params = thisParams;
        SET @rollbackStep = thisRollBackStep;
        SET @executionTime = thisExecutionTime;
        SET @dependency = thisDependency;
        SET @recovery = thisRecoveryStep;
		SET @s = "INSERT INTO tblUnitQueueCommand (`accountID`,`unitID`,`step`,`command`,`params`,`rollbackStep`,`executionTime`,`dependency`,`recoveryStep`) VALUES (?,?,?,?,?,?,?,?,?)";

		PREPARE valid2 FROM @s;

		EXECUTE valid2 USING @accountId,@unitID,@step,@command,@params,@rollbackStep,@executionTime, @dependency, @recovery;
	END IF;
    
END ;

GRANT EXECUTE ON PROCEDURE `sp_tblPermissionAreas_Insert` TO 'newuser'@'localhost';

DROP PROCEDURE IF EXISTS `sp_UpdateCommandAsExecuted`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_UpdateCommandAsExecuted` (
	IN thisAccountID bigint(20),
    IN thisUnitID bigint (20),
    IN thisStep INT)
BEGIN
	UPDATE tblUnitQueueCommand 
		SET `executionTime` = now()
		WHERE `accountID` = thisAccountID
		AND `unitID` = thisUnitID
		AND `step` = thisStep;
END;

DROP VIEW IF EXISTS `view_CommandsForExec`;
CREATE VIEW view_CommandsForExec AS 
	
	SELECT 
		unitCommand.`accountID`,
        unitCommand.`unitID`,
		unitCommand.`step`,
        unitCommand.`dependency`,
        unitCommand.`executionTime`,
		unitCommand.`command`,
		unitCommand.`params`,
		unitCommand.`rollbackStep`,
		unitCommand.`created`,
		unitCommand.`recoveryStep`
	FROM 
		`tblUnitQueueCommand`  unitCommand
	LEFT OUTER JOIN `tblUnitQueueCommand` dependencies
	ON dependencies.accountID = unitCommand.accountID
    AND dependencies.unitID = unitCommand.unitID
    AND dependencies.step = unitCommand.dependency
    	WHERE
		(unitCommand.`recoveryStep` = FALSE
        AND unitCommand.`executed` IS NULL
		AND unitCommand.`executionTime` <=  "2017-01-17 01:01:01"
        AND unitCommand.`processing` = false
        )
        
        AND (
			(unitCommand.`dependency` IS NULL) 
			OR (dependencies.`executed` IS NOT NULL)
		);


DROP PROCEDURE IF EXISTS `sp_UpdateCommandAsProcessing`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_UpdateCommandAsProcessing` (
	IN thisAccountID bigint(20),
    IN thisUnitID bigint (20),
    IN thisStep INT)
BEGIN
	UPDATE tblUnitQueueCommand 
		SET `processing` = true
		WHERE `accountID` = thisAccountID
		AND `unitID` = thisUnitID
		AND `step` = thisStep;
END;

DROP PROCEDURE IF EXISTS `sp_UpdateCommandAsProcessed`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_UpdateCommandAsProcessed` (
	IN thisAccountID bigint(20),
    IN thisUnitID bigint (20),
    IN thisStep INT)
BEGIN
	UPDATE tblUnitQueueCommand 
		SET `processing` = false,
        `executed` = now()
		WHERE `accountID` = thisAccountID
		AND `unitID` = thisUnitID
		AND `step` = thisStep;
END;

DROP TABLE IF EXISTS `requestsIn`;
CREATE TABLE `requestsIn` (
	`request` JSON NOT NULL,
    `received` DATETIME NOT NULL DEFAULT now(),
    `inProgress` BOOLEAN DEFAULT FALSE,
    `serialID` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `processed` BOOLEAN DEFAULT FALSE,
	PRIMARY KEY (`serialID`, `processed`)
)ENGINE = InnoDB;


prepare `loadNewRequestStatement` FROM "INSERT INTO requestsIn (`request`) VALUES (?)";

DROP PROCEDURE IF EXISTS `sp_loadNewRequest`;

DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_loadNewRequest` (
	IN thisJson TEXT
)
BEGIN
	set @newJson = thisJson;

    EXECUTE loadNewRequestStatement USING @newJson;
END;