package wfl.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import wfl.dao.interfaces.QueueCommandsDao;

/**
 * Created by michaelwheeler on 2017/01/23.
 */
@Component
public class UnitQueueCommandsPersistence {
    private  static  QueueCommandsDao queueCommandsDao;


        @Autowired
        public UnitQueueCommandsPersistence(QueueCommandsDao queueCommandsDao) {
            UnitQueueCommandsPersistence.queueCommandsDao = queueCommandsDao;
        }

}
