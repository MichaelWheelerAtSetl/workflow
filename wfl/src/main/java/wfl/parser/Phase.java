package wfl.parser;

/**
 * Created by michaelwheeler on 2017/01/20.
 */

/*
     Prep
     Balance
     P-Nom
     S-Nom
     E-Nom
     Dist
     Exc
     Post
 */
public enum Phase {
    Prep,
    Balance,
    P_Nom,
    S_Nom,
    E_Nom,
    Dist,
    Exc,
    Post
}
