package wfl.parser.Phases.commands;

/**
 * Created by michaelwheeler on 2017/01/20.
 */
public abstract class Command {
        protected int step;
        protected String param;
        protected int rollBack;
        protected Integer dependency;

}
