package wfl.parser.Phases;

import org.json.simple.JSONObject;
import wfl.beans.TblUnitQueueCommandEntity;
import wfl.factories.TasksAndParamsHandler;
import wfl.factories.TemplateHandler;
import wfl.parser.Phases.commands.Command;
import wfl.persistence.UnitQueueCommandsPersistence;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by michaelwheeler on 2017/01/20.
 */
public class P_Nom extends Phase{

    enum Commands {
        CreateSnap,
        Distribute
    }
    public P_Nom(TemplateHandler templateHandler, TasksAndParamsHandler tasksHandler, String phase) {
        super(templateHandler, tasksHandler, phase);
        create();
    }
    public void create() {
         Iterator<Map<String, String>> i =  commands.iterator();
        while (i.hasNext()) {
            Map<String, String> currentCommand = i.next();
            String currentCommandString = currentCommand.get("COMMAND");
            switch (Commands.valueOf(currentCommandString)) {
                case CreateSnap: {
                    createSnap(currentCommand, currentCommandString);
                } break;
                case Distribute:  {
                    createDistribute(currentCommand, currentCommandString);
                }
                break;
            }
        }
    }

    private void createDistribute(Map<String, String> currentCommand, String currentCommandString) {
        TblUnitQueueCommandEntity tblUnitQueueCommandEntity = fetchTblUnitQueueCommandEntity(currentCommandString, currentCommand);

    }

    private void createSnap(Map<String, String>  currentCommand, String currentCommandString) {
        TblUnitQueueCommandEntity tblUnitQueueCommandEntity = fetchTblUnitQueueCommandEntity (currentCommandString, currentCommand);
//'{"Name" : "Bal","Phase" : "Balance","Asset" : "Assets.Base[0]","ColHead" : "Address,Balance","ColSource" : "Address,Balance","ExcludeRow" : "Address=123456789","IncludeRow" : "Balance>0"}'
        String phases_Balance = getBalance();
        String assets_BaseZero = getAssetsBase(0);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Name", "Bal");
        jsonObject.put("Phase", tblUnitQueueCommandEntity.getExecutionTime().toString());
        jsonObject.put("Assets", assets_BaseZero);
        jsonObject.put("ColHead", "Address, Balance, Lots, Residual, AssetOutAmount,ResidualOutAmount");
        jsonObject.put("ColSource", "Address, Balance, Lots, Residual, AssetOutAmount,ResidualOutAmount");
        jsonObject.put("ExcludeRow", "Address=" +tblUnitQueueCommandEntity.getAccountId());
        jsonObject.put("IncludeRow", "Balance>0");
        tblUnitQueueCommandEntity.setParams(jsonObject.toString());
        tblUnitQueueCommandEntities.add(tblUnitQueueCommandEntity);
    }
}
