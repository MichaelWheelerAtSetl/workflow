package wfl.parser.Phases;

import org.json.simple.JSONArray;
import wfl.beans.TblUnitQueueCommandEntity;
import wfl.factories.TasksAndParamsHandler;
import wfl.factories.TemplateHandler;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by michaelwheeler on 2017/01/20.
 */
public abstract class Phase {
    private final TemplateHandler templateHandler;
    private final TasksAndParamsHandler tasksHandler;
    protected List<TblUnitQueueCommandEntity> tblUnitQueueCommandEntities;

    protected JSONArray commands;
    public Phase(TemplateHandler templateHandler, TasksAndParamsHandler tasksHandler, String phase) {
        this.tasksHandler = tasksHandler;
        this.templateHandler = templateHandler;
        commands = templateHandler.getPhase(phase);
        tblUnitQueueCommandEntities = new ArrayList<TblUnitQueueCommandEntity>();
    }
    protected TblUnitQueueCommandEntity fetchTblUnitQueueCommandEntity(String commandString, Map<String, String> currentCommand) {
        TblUnitQueueCommandEntity tblUnitQueueCommandEntity = new TblUnitQueueCommandEntity();
        tblUnitQueueCommandEntity.setAccountId(tasksHandler.getAccountID());
        tblUnitQueueCommandEntity.setCommand(commandString);
        tblUnitQueueCommandEntity.setStep(Integer.parseInt(currentCommand.get("STEP")));
        Integer dependency = null;
        if (!currentCommand.get("DEPENDENCY").equalsIgnoreCase("null")) {
            dependency = Integer.parseInt(currentCommand.get("DEPENDENCY"));
        }
        tblUnitQueueCommandEntity.setDependency(dependency);
        Integer rollback = null;
        if (!currentCommand.get("ROLLBACK").equalsIgnoreCase("null")) {
            rollback = Integer.parseInt(currentCommand.get("ROLLBACK"));
        }
        tblUnitQueueCommandEntity.setRollbackStep(rollback);
        tblUnitQueueCommandEntity.setExecutionTime(Timestamp.valueOf(tasksHandler.getPhases_Balance()));
        return tblUnitQueueCommandEntity;
    }


    public String getBalance() {
        return tasksHandler.getPhases_Balance();
    }

    public String getAssetsBase(int index) {
        return tasksHandler.getAssetsBase(index);
    }
    public String getDistributeAssetOut() {
        return tasksHandler.getAssetOut();
    }
    abstract void create();
    public List<TblUnitQueueCommandEntity> getTblUnitQueueCommandEntities() {
        return this.tblUnitQueueCommandEntities;
    }

}
