package wfl.workflowSteps;

/**
 * Created by michaelwheeler on 2017/01/20.
 */
public class P_Nom {
    public P_Nom(){}
    private String command;
    private int step;
    private String param;

    private String row_exec;
    private Integer dependency;

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getRow_exec() {
        return row_exec;
    }

    public Integer getDependency() {
        return dependency;
    }

    public void setDependency(Integer dependency) {
        this.dependency = dependency;
    }

    public void setRow_exec(String row_exec) {
        this.row_exec = row_exec;
    }
}
