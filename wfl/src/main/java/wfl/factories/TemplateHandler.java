package wfl.factories;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by michaelwheeler on 2017/01/20.
 */
public class TemplateHandler {

    private Map<String, JSONArray> templateParams;
    private Map<String, JSONArray> templateHeader;
    private Map<String, JSONArray> templateAssets;
    private Map<String, JSONArray> templateNominate;
    private Map<String, JSONArray> templateConvert;
    private Map<String, JSONArray> templateWorkFlow;
    public TemplateHandler(String template) {

        JSONParser JSONparser = new JSONParser();
        try {
            JSONObject templateMap = (JSONObject) JSONparser.parse(template);

            templateParams = (Map<String, JSONArray>) templateMap.get("Parameters");
            templateWorkFlow = (Map<String, JSONArray>) templateMap.get("Workflow");
            templateHeader = (Map<String, JSONArray>) templateParams.get("Header");
            templateAssets = (Map<String, JSONArray>) templateParams.get("Assets");
            templateNominate = (Map<String, JSONArray>) templateParams.get("Nominate");
            templateConvert = (Map<String, JSONArray>) templateParams.get("Convert");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public Set<String> getPhases() {
    return templateWorkFlow.keySet();
    }
    public JSONArray getPhase(String phase) {
        return templateWorkFlow.get(phase);
    }

}