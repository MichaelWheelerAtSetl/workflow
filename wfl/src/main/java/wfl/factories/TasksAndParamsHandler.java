package wfl.factories;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Created by michaelwheeler on 2017/01/20.
 */
public class TasksAndParamsHandler {
    static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    private Map<String, Object> taskHeader;
    private Map<String, JSONArray> taskAssets;
    private Map<String, JSONArray> taskDistribute;
    public TasksAndParamsHandler(String tasksAndParams)  {
        JSONParser JSONparser = new JSONParser();

        try {
            JSONObject tasksAndPramsMap = (JSONObject) JSONparser.parse(tasksAndParams);
            taskHeader = (Map<String, Object>) tasksAndPramsMap.get("Header");
            taskAssets = (Map<String, JSONArray>) tasksAndPramsMap.get("Assets");
            taskDistribute = (Map<String, JSONArray>) tasksAndPramsMap.get("Distribute");
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
    public String getPhases_Balance() {
        Map<String, String> phases = (Map<String, String>) taskHeader.get("Phases");
        String balanceDate = phases.get("Balance");
        return balanceDate;
    }
    public String getAssets_Base() {
        JSONArray base = (JSONArray)taskAssets.get("Base");
        return (String)base.get(0);


    }
    public Long getAccountID() {

        Long o = (Long)taskHeader.get("Issuer");
        return o.longValue();
    }

    public String getAssetsBase(int index) {
        JSONArray base = taskAssets.get("Base");
        return (String) base.get(index);

    }

    public Timestamp getExecutionTime() {
        return (Timestamp.valueOf((String)taskHeader.get("Balance")));
    }

    public String getAssetOut() {
        JSONArray temp = taskDistribute.get("Asset out");
        return (String)temp.get(0);
    }
}
